/*
    zandroid.c - Android JNI interface
    Copyright (C) 2012-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <libziaint.h>

#ifdef Z_ANDROID

#include <zandroid.h>

#include <zdebug.h>
#include <zselect.h>

#include <jni.h>
#include <android/log.h>


static JNIEnv *genv;
static jobject gMainActivity;
static jclass gMainActivityClass;

static jmethodID updatePackage;
static jmethodID browser;
static jmethodID playWav;
static jmethodID ssbdAbort;
static jmethodID updateLocation;
static jmethodID messageBox;

struct zselect *zsel_location;

void zandroid_init(struct zselect *location){
    zsel_location = location;
}

JNIEXPORT void JNICALL Java_cz_nagano_tucnak_MainActivity_nativeInitJavaCallbacks(JNIEnv *env, jobject thiz){
    __android_log_print(ANDROID_LOG_DEBUG, "tucnak", "nativeInitJavaCallbacks started env=%p thiz=%p -------------\n", env, thiz);
    genv = env;
    gMainActivity = (*genv)->NewGlobalRef(genv, thiz);
    gMainActivityClass = (*genv)->GetObjectClass(genv, thiz);
    //dbg("genv=%p, gMainActivity=%p, gMainActivityClass=%p\n", genv, gMainActivity, gMainActivityClass);
    updatePackage = (*genv)->GetMethodID(genv, gMainActivityClass, "UpdatePackage", "(Ljava/lang/String;)V");
    browser = (*genv)->GetMethodID(genv, gMainActivityClass, "Browser", "(Ljava/lang/String;)V");
    playWav = (*genv)->GetMethodID(genv, gMainActivityClass, "PlayWav", "(Ljava/lang/String;)V");
    ssbdAbort = (*genv)->GetMethodID(genv, gMainActivityClass, "SsbdAbort", "()V");
    updateLocation = (*genv)->GetMethodID(genv, gMainActivityClass, "UpdateLocation", "()V");
    messageBox = (*genv)->GetMethodID(genv, gMainActivityClass, "MessageBox", "(Ljava/lang/String;Ljava/lang/String;)V");

    __android_log_print(ANDROID_LOG_DEBUG, "tucnak", "nativeInitJavaCallbacks finished\n");
}


void zandroid_update_package(const char *filename){
    dbg("zandroid_update_package('%s')\n", filename);
    //dbg("updatePackage=%p\n", updatePackage);
    jstring jFileName = (*genv)->NewStringUTF(genv, filename);
    //dbg("jFileName=%p\n", jFileName);
    (*genv)->CallVoidMethod(genv, gMainActivity, updatePackage, jFileName);
    dbg("zandroid_update_package return\n");
}

void zandroid_browser(const char *url){
    dbg("zandroid_browser('%s')\n", url);
    //dbg("updatePackage=%p\n", updatePackage);
    jstring jUrl = (*genv)->NewStringUTF(genv, url);
    //dbg("jFileName=%p\n", jFileName);
    (*genv)->CallVoidMethod(genv, gMainActivity, browser, jUrl);
    dbg("zandroid_browser return\n");
}
void zandroid_play_wav(const char *filename){
    dbg("zandroid_play_wav('%s')\n", filename);
    jstring jFileName = (*genv)->NewStringUTF(genv, filename);
    (*genv)->CallVoidMethod(genv, gMainActivity, playWav, jFileName);
    dbg("zandroid_play_wav return\n");
}

void zandroid_ssbd_abort(){
    dbg("zandroid_ssbd_abort()\n");
    (*genv)->CallVoidMethod(genv, gMainActivity, ssbdAbort);
}


static double myH, myW;
static int myState;

JNIEXPORT void JNICALL Java_cz_nagano_tucnak_MainActivity_nativeSendLocation(JNIEnv *env, jobject thiz, jdouble jh, jdouble jw, jint jstate){
    myH = jh;
    myW = jw;
    myState = jstate;
	if (zsel_location) zselect_msg_send(zsel_location, "TERM");
}

void zandroid_get_location(double *h, double *w, int *state){
    *h = myH;
    *w = myW;
    *state = myState;
}

void zandroid_update_location(){
	dbg("zandroid_update_location()\n");
	myState = 0;
    (*genv)->CallVoidMethod(genv, gMainActivity, updateLocation);
}

void zandroid_messagebox(const char *caption, const char *str){
    dbg("zandroid_messagebox('%s', '%s')\n", caption, str);
    jstring jCap = (*genv)->NewStringUTF(genv, caption);
    jstring jStr = (*genv)->NewStringUTF(genv, str);
    (*genv)->CallVoidMethod(genv, gMainActivity, messageBox, jCap, jStr);
    dbg("zandroid_messagebox return\n");
}

#endif
