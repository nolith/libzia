/*
    zclip.c - clipboard functions
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <libziaint.h>
#include <zclip.h>

#include <glib.h>

#ifdef Z_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif
#ifdef Z_HAVE_WINDOWS_H
#include <windows.h>
#endif

int zclip_init(void){
#ifdef Z_MSC_MINGW
    return 0;
#else
    return 0;
#endif
}

#ifdef Z_HAVE_SDL
int zclip_event_filter(const SDL_Event *event){
#ifdef Z_MSC_MINGW
    return 0;
#else
    return 0;
#endif
}
#endif

int zclip_copy(const char *text){
#ifdef Z_MSC_MINGW
    HGLOBAL data;
    int ret, size;

    if (!OpenClipboard(NULL)) return -2;
    EmptyClipboard();
	size = strlen(text) + 1;
    data = GlobalAlloc(GMEM_DDESHARE, size);
    g_strlcpy((char *)data, text, size);
    GlobalUnlock(data);
    ret = SetClipboardData(CF_TEXT, data) == NULL;
    CloseClipboard();
    return 0;
#else
    return -1;
#endif
}

char *zclip_strdup_paste(void){
#ifdef Z_MSC_MINGW
    HANDLE data;
    char *text;

    if (!OpenClipboard(NULL)) return NULL;
    data = GetClipboardData(CF_TEXT);
    if (data == NULL){
        CloseClipboard();
        return NULL;
    }
    text = g_strdup((char *)GlobalLock(data));
    GlobalUnlock(data);
    CloseClipboard();
    return text;
#else
    return NULL;
#endif

}


