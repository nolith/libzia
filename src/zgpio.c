/*
    zgpio.c - GPIO library
    Copyright (C) 2018 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

// https://stackoverflow.com/questions/19257624/interrupt-handling-and-user-space-notification

#include <libziaint.h>
#include <zgpio.h>
#include <zfile.h>
#include <zstr.h>

#include <glib.h>   
#include <stdlib.h>
#include <string.h>

void zgpiochip_free(struct zgpiochip *chip){
    if (chip == NULL) return;

    chip->freechip(chip);

    g_free(chip);
}


struct zgpio *zgpio_init_nr(struct zgpiochip *chip, int nr){
    return chip->init_nr(chip, nr);
}

struct zgpio *zgpio_init_name(struct zgpiochip *chip, const char *name){
    return chip->init_name(chip, name);
}

void zgpio_free(struct zgpio *gpio){
    if (!gpio) return;

    gpio->chip->free(gpio);
    
    g_free(gpio->name);
    g_free(gpio);
}

int zgpio_dir_input(struct zgpio *gpio){
    return gpio->chip->dir_input(gpio);
}


int zgpio_dir_output(struct zgpio *gpio){
    return gpio->chip->dir_output(gpio);
}

int zgpio_write(struct zgpio *gpio, int value){
    return gpio->chip->write(gpio, value);
}

int zgpio_read(struct zgpio *gpio){
    return gpio->chip->read(gpio);
}

int zgpio_set_handler(struct zgpio *gpio, struct zselect *zsel, enum zgpio_edge edge, 
        void (*handler)(struct zgpio *gpio, int value, void *data), 
        void *data){
    return gpio->chip->set_handler(gpio, zsel, edge, handler, data);
}
