/*
    zhdkeyb.c - routines for hdkeyb control
    http://ok1zia.nagano.cz/wiki/Hdkeyb

    Copyright (C) 2010-2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

/* TODO

   pri chybe na USB ukoncit/znovu spustit

   */

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <libziaint.h>

#include <zhdkeyb.h>

#include <zdebug.h>
#include <zselect.h>
#include <zthread.h>

#include <ctype.h>
#include <string.h>

#include <glib.h>
#ifdef Z_HAVE_SDL
#include <SDL.h>
#endif
#ifdef Z_HAVE_LIBPNG
#include <zpng.h>
#endif



#define PIN_HD_E    0x04
#define PIN_HD_RS   0x01
#define PIN_HD_RW   0x10
#define PIN_HD_D4   0x02
#define PIN_HD_D5   0x80
#define PIN_HD_D6   0x20
#define PIN_HD_D7   0x40
#define PIN_HD_BUSY 0x08

#define PIN_KEYB_A0 0x10
#define PIN_KEYB_A1 0x01
#define PIN_KEYB_A2 0x04



#define PIN(pins, value) if (value) zhdkeyb->wr |= (pins); else zhdkeyb->wr &= ~(pins);
struct zhdkeyb *zhdkeyb;

struct zhdkeyb *zhdkeyb_init(struct zselect *zsel){
    struct zhdkeyb *zhdkeyb;


    zhdkeyb = g_new0(struct zhdkeyb, 1);
#ifdef Z_HAVE_LIBFTDI
    zhdkeyb->zsel = zsel;

    //zhdkeyb->thread = g_thread_create(zhdkeyb_main, (gpointer)zhdkeyb, TRUE, NULL); 
    zhdkeyb->thread = g_thread_try_new("zhdkeyb", zhdkeyb_main, (gpointer)zhdkeyb, NULL); 
    if (!zhdkeyb->thread) {
        error("Can't create zhdkeyb thread\n");
        g_free(zhdkeyb);
        return NULL;
    }
	dbg("zhdkeyb started\n");
#endif   
//    zhdkeyb->thread_break = 1;
	//zhdkeyb_dump_vrams(zhdkeyb);
    return zhdkeyb;
}

void zhdkeyb_free(struct zhdkeyb *zhdkeyb){
#ifdef Z_HAVE_LIBFTDI
    if (!zhdkeyb) return;
    if (zhdkeyb->thread){
        zhdkeyb->thread_break = 1;
        dbg("join zhdkeyb...\n");
        g_thread_join(zhdkeyb->thread);
        dbg("done\n");
        zhdkeyb->thread=NULL;
    }
#endif
    g_free(zhdkeyb);
}

#ifdef Z_HAVE_LIBFTDI
void zhdkeyb_dump_vrams(struct zhdkeyb *zhdkeyb){
	int i;
	char vrc;
	dbg("oldv='");
	for (i=0; i<16; i++) {
		vrc = zhdkeyb->oldvram[0][i];
        dbg("%c", isprint((unsigned char)vrc) ? vrc : '.');
	}
	dbg("'\nvram='");
	for (i=0; i<16; i++) {
		vrc = zhdkeyb->vram[0][i];
        dbg("%c", isprint((unsigned char)vrc) ? vrc : '.');
	}
	dbg("'\n");
}

gpointer zhdkeyb_main(gpointer xxx){
    struct zhdkeyb *zhdkeyb;
//    int i;
    int ret, ftdi_vid = 0xa600, ftdi_pid = 0xe114, bitmask;
    zhdkeyb = (struct zhdkeyb*)xxx;

    zhdkeyb->ftdi = ftdi_new();
    if (!zhdkeyb->ftdi){
        zselect_msg_send(zhdkeyb->zsel, "HD;!;Can't create zhdkeyb ftdi");
        return NULL;
    }

    ret = ftdi_usb_open(zhdkeyb->ftdi, ftdi_vid, ftdi_pid);
    if (ret){
        //zselect_msg_send(zhdkeyb->zsel, "HD;!;Can't open ftdi device %04x:%04x, error=%d %s", ftdi_vid, ftdi_pid, ret, ftdi_get_error_string(zhdkeyb->ftdi));
        dbg("HD;!;Can't open ftdi device %04x:%04x, error=%d %s", ftdi_vid, ftdi_pid, ret, ftdi_get_error_string(zhdkeyb->ftdi));
        if (ret==-8) zselect_msg_send(zhdkeyb->zsel, "HD;!;Maybe run program as root");
        return NULL;
    }
            
    bitmask = (PIN_HD_RS | PIN_HD_RW | PIN_HD_E | PIN_HD_D4 | PIN_HD_D5 | PIN_HD_D6 | PIN_HD_D7);
    ret=ftdi_set_bitmode(zhdkeyb->ftdi, bitmask, BITMODE_SYNCBB);
    //dbg("ftdi_set_bitmode(0x%02x)=%d", bitmask, ret);
    if (ret){
        zselect_msg_send(zhdkeyb->zsel, "HD;!;Can't enable bitbang, error=%d %s", ret, ftdi_get_error_string(zhdkeyb->ftdi));
        return NULL;
    }
        
    ret=ftdi_set_baudrate(zhdkeyb->ftdi, 1200);
    if (ret){
        zselect_msg_send(zhdkeyb->zsel, "HD;!;Can't set baudrate for ftdi, error=%d %s", ret, ftdi_get_error_string(zhdkeyb->ftdi));
        return NULL;
    }

    zhdkeyb_reset(zhdkeyb);
    zhdkeyb_clear(zhdkeyb);
#if 0 && defined(Z_HAVE_SDL) && defined(Z_HAVE_LIBPNG)
    SDL_Surface *png = z_png_create(icon_tucnak23, sizeof(icon_tucnak23));
    if (png) zinternal("Can't create icon_tucnak23, corrupted executable?");

    zhdkeyb_setdir(zhdkeyb); 	
    zhdkeyb_cursor(zhdkeyb, 0);
    for (i = 0; i < 4; i++) zhdkeyb_data(zhdkeyb, i);
    zhdkeyb_cursor(zhdkeyb, 64);
    for (i = 4; i < 8; i++) zhdkeyb_data(zhdkeyb, i);
    zhdkeyb_print(zhdkeyb, 5, "Tucnak");
    zhdkeyb_print(zhdkeyb, 12, PACKAGE_VERSION);
    zhdkeyb_print(zhdkeyb, 64+5, "(C) OK1ZIA");
    zhdkeyb_flush(zhdkeyb);

    for (i=png->h - 17; i>=0; i--){
        int j, x, y, d, k;
        zhdkeyb_cgram(zhdkeyb, 0);

        for (j=0; j<64; j++){
            x = ((j % 32) / 8) * 6;
            y = (j / 32) * 9 + j % 8 + i;
            
            
            d = 0;
   //         dbg("j=%d x=%d y=%d\n", j, x, y);
            for (k=0; k<5; k++){
                if (x + k >= png->w) continue;
                if (y >= png->h) continue;
                if (z_getpixel(png, 3*(x + k), y) == 0) {
                    d |= 1 << (4 - k); 
                    /*fast_putpixel(sdl->screen, x + k, y - i, makecol(255, 255, 255));
                }else{
                    fast_putpixel(sdl->screen, x + k, y - i, makecol(40, 40, 40));*/
                }
            }
            zhdkeyb_data(zhdkeyb, d);
        } 
        zhdkeyb_flush(zhdkeyb);
        /*for (x = 0; x < png->w; x++)
            for (y=0; y < png->h; y++){
                if (fast_getpixel8(png, x*3, y) == 0) {
                    fast_putpixel(sdl->screen, x + 30, y, makecol(255, 255, 255));
                }else{
                    fast_putpixel(sdl->screen, x + 30, y, makecol(40, 40, 40));
                }
                fast_putpixel(sdl->screen, x + 60, y, fast_getpixel8(png, 3*x, y));
            }
        SDL_Rect r;
        r.x = 90;
        r.y = 0;
        r.w = 50;;
        r.h = 50;
        SDL_BlitSurface(png, NULL, sdl->screen, &r);
        SDL_UpdateRect(sdl->screen, 0, 0, sdl->screen->w, sdl->screen->h);*/
        usleep(150000);  
    }
    sleep(2);
#endif

    zhdkeyb_clear(zhdkeyb);
	zhdkeyb_setdir(zhdkeyb); 	
    zhdkeyb_cgram(zhdkeyb, 8);
    zhdkeyb_data(zhdkeyb, 0x1c); // degree
    zhdkeyb_data(zhdkeyb, 0x14);
    zhdkeyb_data(zhdkeyb, 0x1c);
    zhdkeyb_data(zhdkeyb, 0x00);
    zhdkeyb_data(zhdkeyb, 0x00);
    zhdkeyb_data(zhdkeyb, 0x00);
    zhdkeyb_data(zhdkeyb, 0x00);
    zhdkeyb_data(zhdkeyb, 0x00);
    zhdkeyb_data(zhdkeyb, 0x08); // right arrow
    zhdkeyb_data(zhdkeyb, 0x0c);
    zhdkeyb_data(zhdkeyb, 0x0e);
    zhdkeyb_data(zhdkeyb, 0x1f);
    zhdkeyb_data(zhdkeyb, 0x0e);
    zhdkeyb_data(zhdkeyb, 0x0c);
    zhdkeyb_data(zhdkeyb, 0x08);
    zhdkeyb_data(zhdkeyb, 0x00);
    zhdkeyb_data(zhdkeyb, 0x02); // left arrow
    zhdkeyb_data(zhdkeyb, 0x06);
    zhdkeyb_data(zhdkeyb, 0x0e);
    zhdkeyb_data(zhdkeyb, 0x1f);
    zhdkeyb_data(zhdkeyb, 0x0e);
    zhdkeyb_data(zhdkeyb, 0x06);
    zhdkeyb_data(zhdkeyb, 0x02);
    zhdkeyb_data(zhdkeyb, 0x00);
	zhdkeyb_flush(zhdkeyb);

    while(!zhdkeyb->thread_break){
        int li, co;
		char key;

        g_thread_yield();

        key = zhdkeyb_read_key(zhdkeyb);
        if (key){
            zselect_msg_send(zhdkeyb->zsel, "HD;k;%c", key);
        }

        for (li = 0; li < Z_HDKEYB_LINES; li++){
            for (co = 0; co < Z_HDKEYB_CHARS; co++){
                char vrc = zhdkeyb->vram[li][co];
                if (vrc == zhdkeyb->oldvram[li][co]) continue;

         //       dbg("1 li=%d co=%d old='%c' c='%c'\n", li, co, zhdkeyb->oldvram[li][co], vrc);

			    zhdkeyb_setdir(zhdkeyb); 	
                zhdkeyb_cursor(zhdkeyb, li * 64 + co);
                zhdkeyb_data(zhdkeyb, vrc);
                zhdkeyb->oldvram[li][co] = vrc;
                for (co++; co < Z_HDKEYB_CHARS; co++){
                    vrc = zhdkeyb->vram[li][co];
           //     	dbg("2 li=%d co=%d old='%c' c='%c'\n", li, co, zhdkeyb->oldvram[li][co], vrc);
                    if (vrc == zhdkeyb->oldvram[li][co]) break;
                    zhdkeyb_data(zhdkeyb, vrc);
                    zhdkeyb->oldvram[li][co] = vrc;
                }
				zhdkeyb_flush(zhdkeyb);
            }
        }


    }
    ftdi_set_bitmode(zhdkeyb->ftdi, 0x00, BITMODE_RESET);
    ftdi_free(zhdkeyb->ftdi);
    return NULL;
}



/*static long int difftimeval_ms(struct timeval *stop, struct timeval *start){
    int sec, usec; 

    usec = stop->tv_usec - start->tv_usec;
    sec = stop->tv_sec - start->tv_sec;
    if (usec < 0){
        usec += 1000000;
        sec --;
    }
    if (sizeof(long) == 4){
        if (sec > 2000000) sec = 2000000;
    }
    if (sizeof(long) == 2){
        if (sec > 63) sec = 63;
    }
    return (long)sec * 1000L + (long)usec / 1000L;
}  */






void zhdkeyb_send(struct zhdkeyb *zhdkeyb){
//    dbg("zhdkeyb_send(%x) slen=%d\n", (unsigned char)zhdkeyb->wr, zhdkeyb->slen);
    zhdkeyb->sbuf[zhdkeyb->slen++] = zhdkeyb->wr;
    if (zhdkeyb->slen==Z_HDKEYB_BUFLEN) zhdkeyb_flush(zhdkeyb);
}

int zhdkeyb_flush(struct zhdkeyb *zhdkeyb){
    int ret;
    int l;

    if (!zhdkeyb->slen) return 0;

    l = zhdkeyb->slen;
    zhdkeyb->slen = 0;
    zhdkeyb->rlen = 0;

//    dbg("zhdkeyb_flush() slen=%d\n", l);
    ret = ftdi_write_data(zhdkeyb->ftdi, (unsigned char*)zhdkeyb->sbuf, l);
    if (ret != l){
        zselect_msg_send(zhdkeyb->zsel, "HD;!;ftdi_write_data;%s", ftdi_get_error_string(zhdkeyb->ftdi));
        return -1;
    }
#if 0
    int i;
    for (i=0; i<l; i++) zhdkeyb_debug_pins(zhdkeyb->sbuf[i], "f");    
#endif
#if 0
    { int i;
    dbg("w ");
    for (i=0; i<ret; i++) dbg("%02x ", (unsigned char)zhdkeyb->sbuf[i]);    
    dbg("\n"); }
#endif

    ret = ftdi_read_data(zhdkeyb->ftdi, (unsigned char *)zhdkeyb->rbuf, l);
    if (ret < 0){
        zselect_msg_send(zhdkeyb->zsel, "HD;!;ftdi_read_data;%s", ftdi_get_error_string(zhdkeyb->ftdi));
        return -1;
    }
#if 0
    for (i=0; i<ret; i++) zhdkeyb_debug_pins(zhdkeyb->sbuf[i], "f");    
#endif
#if 0
    {int i;
    dbg("r ");
    for (i=0; i<ret; i++) dbg("%02x ", (unsigned char)zhdkeyb->sbuf[i]);    
    dbg("\n");}
#endif
    zhdkeyb->rlen = ret;

    return 0;
}

int zhdkeyb_reset(struct zhdkeyb *zhdkeyb){
    int ret;
    int d = 1;

    zhdkeyb->wr &= ~(PIN_HD_D7|PIN_HD_D6|PIN_HD_D5|PIN_HD_D4|PIN_HD_E|PIN_HD_RW|PIN_HD_RS);
    zhdkeyb->wr |= PIN_HD_BUSY;
    
//    dbg("zhdkeyb_reset\n");
    zhdkeyb_send(zhdkeyb);
    zhdkeyb_flush(zhdkeyb);
    usleep(16000*d); // more than 15 ms

    ret = zhdkeyb_cmd_nowait(zhdkeyb, 0x3);  // - - - -   0 0 1 DL 
    if (ret) return ret;
    zhdkeyb_flush(zhdkeyb);
    usleep(5000*d);  // more than 4.1 ms

    ret = zhdkeyb_cmd_nowait(zhdkeyb, 0x3);
    if (ret) return ret;
    zhdkeyb_flush(zhdkeyb);
    usleep(1000*d);  // more than 100 us

    ret = zhdkeyb_cmd_nowait(zhdkeyb, 0x3);
    if (ret) return ret;
    zhdkeyb_flush(zhdkeyb);
    usleep(6000*d);  

    ret = zhdkeyb_cmd_nowait(zhdkeyb, 0x2);  // - - - -   0 0 1 DL    datalen
    if (ret) return ret;
    zhdkeyb_flush(zhdkeyb);
    usleep(10000*d);

    ret = zhdkeyb_cmd(zhdkeyb, 0x28); // 0 0 1 DL  N F - -     datalen numberlines font
    if (ret) return ret;
    ret = zhdkeyb_cmd(zhdkeyb, 0x0c); // 0 0 0 0   1 D C B     displayon cursoron blinkcursor
    if (ret) return ret;
    ret = zhdkeyb_cmd(zhdkeyb, 0x01); // 0 0 0 0   0 0 0 1     clear display, DDRAM addr=0
    if (ret) return ret;
    ret = zhdkeyb_cmd(zhdkeyb, 0x06); // 0 0 0 0   0 1 I/D S   increment shift
    if (ret) return ret;

    zhdkeyb_flush(zhdkeyb);
    return 0;
}

int zhdkeyb_setdir(struct zhdkeyb *zhdkeyb){
    int ret;

    int bitmask = (PIN_HD_RS | PIN_HD_RW | PIN_HD_E | PIN_HD_D4 | PIN_HD_D5 | PIN_HD_D6 | PIN_HD_D7);
    ret = ftdi_set_bitmode(zhdkeyb->ftdi, bitmask, BITMODE_SYNCBB);
    if (ret){
        zselect_msg_send(zhdkeyb->zsel, "HD;!;ftdi_set_birmode;%s", ftdi_get_error_string(zhdkeyb->ftdi));
        return ret;
    }
    return ret;
}

int zhdkeyb_cmd_nowait(struct zhdkeyb *zhdkeyb, char c){
//    int ret;  
    
//    dbg("zhdkeyb_cmd_nowait(%02x)\n", (unsigned char)c);
    PIN(PIN_HD_RS, 0);
    PIN(PIN_HD_RW, 0);
    PIN(PIN_HD_E, 0);
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_HD_D4, c & 0x01);
    PIN(PIN_HD_D5, c & 0x02);
    PIN(PIN_HD_D6, c & 0x04);
    PIN(PIN_HD_D7, c & 0x08);
    PIN(PIN_HD_E, 1);
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_HD_E, 0);
    zhdkeyb_send(zhdkeyb);

    return 0;
}

int zhdkeyb_write(struct zhdkeyb *zhdkeyb, char c){
//    int ret;

//    dbg("zhdkeyb_write(%02x) slen=%d\n", (unsigned char)c, zhdkeyb->slen);
    PIN(PIN_HD_RW, 0);
    PIN(PIN_HD_E, 0);
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_HD_D4, c & 0x10);
    PIN(PIN_HD_D5, c & 0x20);
    PIN(PIN_HD_D6, c & 0x40);
    PIN(PIN_HD_D7, c & 0x80);
    PIN(PIN_HD_E, 1);
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_HD_E, 0);
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_HD_D4, c & 0x01);
    PIN(PIN_HD_D5, c & 0x02);
    PIN(PIN_HD_D6, c & 0x04);
    PIN(PIN_HD_D7, c & 0x08);
    PIN(PIN_HD_E, 1);
    zhdkeyb_send(zhdkeyb);
    
    PIN(PIN_HD_E, 0);
    zhdkeyb_send(zhdkeyb);

    return 0;
}

int zhdkeyb_wait(struct zhdkeyb *zhdkeyb){
//    int busy = 1;
//    struct timeval start, stop;
//    long int diff;

    //usleep(2000);
    return 0;
#if 0
    gettimeofday(&start, NULL);
    while(busy){
        PIN(PIN_HD_RS, 0);
        PIN(PIN_HD_RW, 1); // read
        PIN(PIN_HD_E, 0);
        zhdkeyb_send(zhdkeyb);
        
        PIN(PIN_HD_E, 1);
        zhdkeyb_send(zhdkeyb);

        PIN(PIN_HD_E, 0);
        zhdkeyb_send(zhdkeyb);
        
        PIN(PIN_HD_E, 1);
        zhdkeyb_send(zhdkeyb);

        PIN(PIN_HD_E, 0);
        zhdkeyb_send(zhdkeyb);
        busy = zhdkeyb->rd & PIN_HD_BUSY;

        gettimeofday(&stop, NULL);
        diff = difftimeval_ms(&stop, &start);
        if (diff > 5) {
            error("zhdkeyb_wait: no BUSY reply\n");
            break;
        }
    }
    return 0;
#endif
}
                                

int zhdkeyb_cmd(struct zhdkeyb *zhdkeyb, char c){
    int ret;

#if 0
	if (c & 0x80)
	    dbg("zhdkeyb_cursor(0x%02d)\n", ((unsigned char)c)&0x7f);
	else if (c & 0x80)
		dbg("zhdkeyb_cgram(0x%02x)\n", ((unsigned char)c)&0x3f);
	else
		dbg("zhdkeyb_cmd(0x%02x)\n", (unsigned char)c);
#endif

    PIN(PIN_HD_RS, 0);
    ret = zhdkeyb_write(zhdkeyb, c);
    if (ret) return ret;

    ret = zhdkeyb_wait(zhdkeyb);
    if (ret) return ret;
    return 0;
}

int zhdkeyb_data(struct zhdkeyb *zhdkeyb, char c){
    int ret;

//    dbg("zhdkeyb_data(0x%02x)\n", (unsigned char)c);
    PIN(PIN_HD_RS, 1);
    ret = zhdkeyb_write(zhdkeyb, c);
    if (ret) return ret;

    ret = zhdkeyb_wait(zhdkeyb);
    if (ret) return ret;

    return 0;
}

void zhdkeyb_cgram(struct zhdkeyb *zhdkeyb, char pos) {
    zhdkeyb_cmd(zhdkeyb, 0x40|(pos&0x7f));
    return;
}

void zhdkeyb_printc(struct zhdkeyb *zhdkeyb, char *str){
    zhdkeyb_setdir(zhdkeyb);
    while (*str) {
        zhdkeyb_data(zhdkeyb, *str);
        str++;
    }
    zhdkeyb_flush(zhdkeyb);
} 

void zhdkeyb_printf(struct zhdkeyb *zhdkeyb, char line, char col, char *m, ...){
    va_list l;
    int li, co;
    char *s, *c;
    
    if (line < 0 || col < 0) return;

    va_start(l, m);
    s = g_strdup_vprintf(m, l);
    va_end(l);

    li = line;
    co = col;
    for (c = s; *c != '\0'; c++){
        if (*c == '\n'){
            li++;
            co = 0;
        }
        if (li < Z_HDKEYB_LINES && co < Z_HDKEYB_CHARS){
            zhdkeyb->vram[li][co++] = *c;
        }
    }

    g_free(s);
}
void zhdkeyb_print(struct zhdkeyb *zhdkeyb, char pos, char *str){
    zhdkeyb_setdir(zhdkeyb);
    zhdkeyb_cursor(zhdkeyb, pos);
    while(*str){
        zhdkeyb_data(zhdkeyb, *str);
        str++;
        pos++;
#ifdef MC1601       
        if (pos==0+8) zhdkeyb_cursor(zhdkeyb, pos=64);
        if (pos==64+8) zhdkeyb_cursor(zhdkeyb, pos=0);
#endif      
    }
    zhdkeyb_flush(zhdkeyb);
}

void zhdkeyb_clear(struct zhdkeyb *zhdkeyb){
    unsigned char i;
    
    zhdkeyb_cursor(zhdkeyb, Z_LINE_1);
    for (i=20;i;i--) {
        zhdkeyb_data(zhdkeyb, ' ');
    }
    
    zhdkeyb_cursor(zhdkeyb, Z_LINE_2);
    for (i=20;i;i--) {
        zhdkeyb_data(zhdkeyb, ' ');
    }
}


/* keyboard routines */
char zhdkeyb_keyb_state(struct zhdkeyb *zhdkeyb){
    int ret, cnt;
    char scan;

    int bitmask = (PIN_HD_RS | PIN_HD_RW | PIN_HD_E );
    ret = ftdi_set_bitmode(zhdkeyb->ftdi, bitmask, BITMODE_SYNCBB);
    if (ret){
        zselect_msg_send(zhdkeyb->zsel, "HD;!;ftdi_set_bitmode;%s", ftdi_get_error_string(zhdkeyb->ftdi));
        return 0xff;
    }
    zhdkeyb_flush(zhdkeyb);
    PIN(PIN_HD_D4,1);
    PIN(PIN_HD_D5,1);
    PIN(PIN_HD_D6,1);
    PIN(PIN_HD_D7,1);
    PIN(PIN_HD_BUSY,1);
    PIN(PIN_KEYB_A0,0);
    PIN(PIN_KEYB_A1,0);
    PIN(PIN_KEYB_A2,0);
    PIN(PIN_HD_E,0);  // already 0
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_KEYB_A0,1);
    PIN(PIN_KEYB_A1,0);
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_KEYB_A0,0);
    PIN(PIN_KEYB_A1,1);
    zhdkeyb_send(zhdkeyb);

    PIN(PIN_KEYB_A0,1);
    PIN(PIN_KEYB_A1,1);
    zhdkeyb_send(zhdkeyb);
    zhdkeyb_send(zhdkeyb);
    zhdkeyb_flush(zhdkeyb);

    cnt = 0;
    scan = 0xff;
    
    if (!(zhdkeyb->rbuf[1] & PIN_HD_D4)) { scan = 'A'; cnt++; }
    if (!(zhdkeyb->rbuf[1] & PIN_HD_D5)) { scan = '3'; cnt++; }
    if (!(zhdkeyb->rbuf[1] & PIN_HD_D6)) { scan = '2'; cnt++; }
    if (!(zhdkeyb->rbuf[1] & PIN_HD_D7)) { scan = '1'; cnt++; }

    if (!(zhdkeyb->rbuf[2] & PIN_HD_D4)) { scan = 'B'; cnt++; }
    if (!(zhdkeyb->rbuf[2] & PIN_HD_D5)) { scan = '6'; cnt++; }
    if (!(zhdkeyb->rbuf[2] & PIN_HD_D6)) { scan = '5'; cnt++; }
    if (!(zhdkeyb->rbuf[2] & PIN_HD_D7)) { scan = '4'; cnt++; }

    if (!(zhdkeyb->rbuf[3] & PIN_HD_D4)) { scan = 'C'; cnt++; }
    if (!(zhdkeyb->rbuf[3] & PIN_HD_D5)) { scan = '9'; cnt++; }
    if (!(zhdkeyb->rbuf[3] & PIN_HD_D6)) { scan = '8'; cnt++; }
    if (!(zhdkeyb->rbuf[3] & PIN_HD_D7)) { scan = '7'; cnt++; }

    if (!(zhdkeyb->rbuf[4] & PIN_HD_D4)) { scan = 'D'; cnt++; }
    if (!(zhdkeyb->rbuf[4] & PIN_HD_D5)) { scan = '#'; cnt++; }
    if (!(zhdkeyb->rbuf[4] & PIN_HD_D6)) { scan = '0'; cnt++; }
    if (!(zhdkeyb->rbuf[4] & PIN_HD_D7)) { scan = '*'; cnt++; }
//    dbg("%d %c\n", scan, scan);

    if (cnt != 1) return 0xff;
    return scan;
}

char zhdkeyb_read_key(struct zhdkeyb *zhdkeyb){
    char state;

    state = zhdkeyb_keyb_state(zhdkeyb);
    if (state == zhdkeyb->oldkeystate) return '\0';
    zhdkeyb->oldkeystate = state;
    return state;
}

int zhdkeyb_debug_pins(char a, char *t){
    dbg("%s E=%d  RS=%d  RW=%d  D=%d  BUSY=%d\n", 
            t,
            !!(a & PIN_HD_E),
            !!(a & PIN_HD_RS),
            !!(a & PIN_HD_RW),
            (!!(a & PIN_HD_D4))+ 
            (!!(a & PIN_HD_D5))*2+ 
            (!!(a & PIN_HD_D6))*4+ 
            (!!(a & PIN_HD_D7))*8, 
            !!(a & PIN_HD_BUSY));
    return 0;
}
#else
void zhdkeyb_printf(struct zhdkeyb *zhdkeyb, char line, char col, char *m, ...){
}
#endif
