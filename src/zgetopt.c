/*
    zgetopt.c - getopt compatibility layer
    Copyright (C) 2011-2018 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#include <stdio.h>
#include <string.h>

#include <zgetopt.h>
#ifndef Z_HAVE_GETOPT_H

char *optarg;
int optind = 1;
int opterr = 1;
int optopt = '?';

int getopt(int argc, char *argv[], char *optstring){
	char c;
	char *cc;

	if (optind >= argc) return -1;
	optarg = argv[optind];
	if (optarg[0] != '-') return -1;
	
	c = optarg[1];
	cc = strchr(optstring, c);
	if (cc == NULL){
		optopt = c;
		optind++;
		return '?';
	}
	optarg += 2;
	if (*optarg == '\0') {
		if (cc[1] == ':'){
			optind++;
	        optarg = argv[optind];
		}else{
			optarg = NULL;
		}
	}
	
	optind++;
	return c;
}

#endif
