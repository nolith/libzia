/*
    zssd1306.h - header for SSD1306 display
    Copyright (C) 2016 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZSSD1306_H
#define __ZSSD1306_H

#include <stddef.h>

struct zbusdev;

#define ZSSD1306_SLAVE 0x3c

#ifdef Z_UNIX
int zssd1306_init(struct zbusdev *dev);
void zssd1306_data(struct zbusdev *dev, void *data, size_t len);
void zssd1306_free(struct zbusdev *dev);
void zssd1306_flip(struct zbusdev *dev, char flip);
void zssd1306_invert(struct zbusdev *dev, char invert);
void zssd1306_contrast(struct zbusdev *dev, unsigned char contrast);
void zssd1306_goto(struct zbusdev *dev, unsigned char x, unsigned char y);
#endif

#endif
