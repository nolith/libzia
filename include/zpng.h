/*
    zpng.h - PNG functions for SDL surfaces
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZPNG_H
#define __ZPNG_H

#include <libziaint.h>

#include <zbinbuf.h>

#include <glib.h>

struct SDL_Surface;

struct SDL_Surface *zpng_load(const char *filename);
struct SDL_Surface *zpng_create(const void *data, int len);

int zpng_save(struct SDL_Surface * surf, char * fname, struct zbinbuf *bbuf);

#ifdef Z_HAVE_LIBPNG
void zpng_get_version(GString *gs);
#endif

#endif
