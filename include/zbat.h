/*
    Tucnak - VHF contest log
    Copyright (C) 2012-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZBAT_H
#define __ZBAT_H

#include <libziaint.h>
#include <zsdl.h>


struct zbat {
    int n;
    int capacity;
    char *technology;

    int charging;
	int discharging;
};

struct zbat *zbat_init(void);
void zbat_free(struct zbat *);

void zbat_getinfo(struct zbat *zbat);
#ifdef Z_HAVE_SDL
void zbat_draw(struct zbat *bat, SDL_Surface *surface, int x, int y, int w, int h);
#endif


#endif
