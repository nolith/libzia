/*
    zerror.h - header for zerror.c
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZERROR_H
#define __ZERROR_H

#include <libziaint.h>

#include <errno.h>
#include <glib.h>

#ifdef Z_MSC_MINGW
char *strerror_r(int err_no, char *buf, size_t size);
#endif

char *z_strdup_strerror(int err);
void z_strerror(GString *gs, int err);

#ifdef Z_MSC_MINGW_CYGWIN
void z_lasterror(GString *gs);
void z_lasterror_e(GString *gs, int err);
#endif

const char *z_host_error(void);

#ifdef Z_MSC_MINGW
#define z_sock_errno WSAGetLastError()
#else
#define z_sock_errno errno
#endif

#define z_sock_strerror() z_sock_strerror_func(errbuf, sizeof(errbuf))
const char *z_sock_strerror_func(char *errbuf, int size);
#endif
