/*
    zhash.h - libzia hash extension
    Copyright (C) 2011-2013 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __GHASH_H
#define __GHASH_H

#include <libziaint.h>
#include <glib.h>

typedef struct _ZHashNode      ZHashNode;
typedef struct _ZHashTable     ZHashTable;

struct _ZHashNode
{
  gpointer key;
  gpointer value;
  ZHashNode *next;
};

struct _ZHashTable
{
  gint size;
  gint nnodes;
  guint frozen;
  ZHashNode **nodes;
  GHashFunc hash_func;
  GCompareFunc key_compare_func;
};

/*void init_ghash(void);
void free_ghash(void);*/
ZHashTable *z_hash_table_new (GHashFunc hash_func, GCompareFunc key_compare_func);
void z_hash_table_destroy (ZHashTable *hash_table);
void z_hash_table_foreach (ZHashTable *hash_table, GHFunc func, gpointer user_data);
void z_hash_table_insert (ZHashTable *hash_table, gpointer key, gpointer value);
void z_hash_table_remove (ZHashTable *hash_table, gconstpointer key);
gpointer z_hash_table_lookup (ZHashTable *hash_table, gconstpointer key);
gboolean z_hash_table_lookup_extended (ZHashTable *hash_table, gconstpointer lookup_key, gpointer *orig_key, gpointer *value);
guint z_hash_table_foreach_remove (ZHashTable *hash_table, GHRFunc func, gpointer user_data);
guint z_hash_table_size (ZHashTable *hash_table);


gboolean free_gpointer_item(gpointer key, gpointer value, gpointer user_data); 

#ifndef Z_HAVE_G_HASH_TABLE_REMOVE_ALL
void g_hash_table_remove_all(GHashTable *hash_table);
#endif

#endif
