
/*
    zhttp.h - http client
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZHTTP_H
#define __ZHTTP_H

#include <zasyncdns.h>
#include <zbinbuf.h>
#include <zselect.h>

#include <glib.h>

#ifdef Z_HAVE_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#endif

	
enum zhttp_state{
	ZHTTPST_NEW,
	ZHTTPST_DNS,
	ZHTTPST_CONNECTING,
	ZHTTPST_TLS_HANDSHAKE,
	ZHTTPST_REQUEST,
	ZHTTPST_HEADER,
	ZHTTPST_DATA,
	ZHTTPST_DONE,
	ZHTTPST_ERROR
};

struct zhttp{
    void (*callback)(struct zhttp *);
	void *arg;
    struct zbinbuf *request;
    struct zbinbuf *response;
    struct zasyncdns *adns; 
	struct zselect *zsel;
	char *url;
    char *errorstr;
    char *server;
	char *serveraddr;
    int port;
    char *page;
    int sock;
	int status; // HTTP status
	int dataofs;
	int sent; // sent data
	int origreqlen; // original length of request
	enum zhttp_state state;
	GPtrArray *posts;
	GHashTable *cookies;
	GHashTable *headers;
	char *datastr; // decoded data, freeed with object
	int connecting_timer_id;
#ifdef Z_HAVE_GNUTLS
	int istls;
	gnutls_session_t session;
#endif
};

struct zhttp_post_var{
	char *name;
	char *value;
	char *filename;
	char *localfilename;
};

struct zhttp *zhttp_init(void);
void zhttp_free(struct zhttp *http);
#define zhttp_free0(http) { zhttp_free(http); http = NULL; }

void zhttp_get(struct zhttp *http, struct zselect *zsel, const char *url, void (*callback)(struct zhttp *), void *arg);
void zhttp_add_header(struct zhttp *http, const char *name, const char *value);
void zhttp_post_free(struct zhttp *http);
void zhttp_post_add(struct zhttp *http, const char *name, const char *value);
void zhttp_post_add_file_mem(struct zhttp *http, const char *name, const char *filename, const char *value);
void zhttp_post_add_file_disk(struct zhttp *http, const char *name, const char *filename, const char *localfilename);
void zhttp_post(struct zhttp *http, struct zselect *zsel, const char *url, void (*callback)(struct zhttp *), void *arg);
void zhttp_raw(struct zhttp *http, struct zselect *zsel, const char *url, const char *raw_request,  void (*callback)(struct zhttp *), void *arg);

void zhttp_status(struct zhttp *http, GString *gs);
int zhttp_write_data(struct zhttp *http, const char *filename);

void zhttp_store_cookies(struct zhttp *http, const char *data, int len);

char *http_get_data(struct zhttp *http);

void zhttp_post_json(struct zhttp *http, struct zselect *zsel, const char *url, const char *json, void (*callback)(struct zhttp *), void *arg);

#ifdef Z_HAVE_GNUTLS
void zhttp_init_tls(void);
#endif

char *http_get_header(struct zhttp *http, const char *header_name);
int http_is_content_type(struct zhttp *http, const char *content_type);


#endif
