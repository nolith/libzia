/*
    zdir.h
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZDIR_H
#define __ZDIR_H

#include <libziaint.h>

#ifdef Z_HAVE_IO_H
#include <io.h>
#endif

#include <sys/types.h>
/*#ifdef Z_HAVE_WINDOWS_H
#include <windows.h>
#endif*/

#ifdef Z_HAVE_DIRECT_H
#include <direct.h>
#endif

#ifdef Z_HAVE_UNISTD_H
#include <unistd.h>
#endif

#define Z_MAX_PATH 260

#ifdef Z_HAVE_DIRENT_H

#include <dirent.h>

#else

#define __S_IFMT        0170000 
#define __S_IFDIR       0040000
#define __S_IFREG       0100000

#define __S_ISTYPE(mode, mask)  (((mode) & __S_IFMT) == (mask))
#define S_ISDIR(mode)    __S_ISTYPE((mode), __S_IFDIR)
#define S_ISREG(mode)    __S_ISTYPE((mode), __S_IFREG)


typedef struct dirent{
	char d_name[Z_MAX_PATH];
};

typedef struct DIR{
	long                handle; /* -1 for failed rewind */
    struct _finddata_t  info;
    struct dirent       result; /* d_name null iff first time */
    char                *name;  /* null-terminated char string */
}DIR;


DIR *opendir(char *name);
struct dirent *readdir(DIR *dir);
int closedir(DIR *dir);

#endif

#ifdef Z_MSC
#define zchdir(dir) _chdir(dir)
#else
#define zchdir(dir) chdir(dir)
#endif

int z_scandir (char *dir, struct dirent ***namelist, int (*select) (const char *, const struct dirent *), int (*cmp) (const void *, const void *));
void z_free_namelist(struct dirent ***namelist, int *namelistlen);
int z_select_dir_func(const char *, const struct dirent *de);
int z_select_file_func(const char *, const struct dirent *de);
int z_compare_name_func(const void *d1, const void *d2);
int z_scandir_alphasort(const void *a, const void *b);

int z_mkdir(const char *s, int mode);
int z_mkdir_p(const char *s, int mode);
int z_fmkdir_p(const char *filename, int mode);


#endif
