/*
    zsock.h - socket functions
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZSOCK_H
#define __ZSOCK_H

#include <libziaint.h>

#ifdef Z_HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#ifdef Z_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif

#ifdef Z_HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#ifdef Z_HAVE_WS2TCPIP_H
#include <ws2tcpip.h>
#endif

/*#ifdef Z_HAVE_W32API_WS2TCPIP_H
#include <w32api/ws2tcpip.h>
#endif

#ifdef Z_HAVE_CYGWIN_IN_H
#include <cygwin/in.h>
#endif*/


#ifdef Z_HAVE_UNISTD_H
#include <unistd.h> // for close() prototype
#endif

#include <glib.h>

// to have structure with maximal size of supported protocols
typedef union zsockaddr{
    struct sockaddr sa;
    struct sockaddr_in in;
#ifdef AF_INET6
    struct sockaddr_in6 in6;
#endif
}zsockaddr;

#ifdef Z_MSC_MINGW
typedef int socklen_t;
#endif

int z_sock_nonblock(int fd, int nonblock);
int z_sock_reuse(int fd, int reuseaddr);
int z_sock_broadcast(int fd, int reuseaddr);
int z_sock_error(int fd);
void *z_sockadr_get_addr(union zsockaddr *sa);
int z_sockadr_get_len(union zsockaddr *sa);
char *z_sock_ntoa(GString *gs, int family, union zsockaddr *sa);
int z_sock_aton(const char *hostname, int port, union zsockaddr *sa);

int z_pipe(int fds[2]);

#ifdef Z_MSC_MINGW
int inet_aton(const char *cp, struct in_addr *inp);
#endif



int z_sock_wouldblock(int err);
int z_sock_printf(int fd, const char *m, ...);

#ifdef Z_UNIX_ANDROID
#define closesocket close
#endif


int z_pipe_write(int fd, const void *buf, int count);
int z_pipe_read(int fd, void *buf, int count);
int z_pipe_close(int fd);


void z_sock_set_errno(int err);
int z_sock_connect(int sock, union zsockaddr *sa, int timeout_ms);

#endif
