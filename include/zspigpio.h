/*
    zspidev.c - SPI over spidev
    Copyright (C) 2018 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/
struct zgpio;

struct zbusdev *zspigpio_init(struct zgpio *sclk, struct zgpio *mosi, struct zgpio *miso, struct zgpio *ss, int sleep_us, int sspol);
