/*
    interfaces - network interfaces detection
    Copyright (C) 2002-20011  Ladislav Vaiz <ok1zia@nagano.cz>
    and authors of samba www.samba.org

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.
*/


#ifndef _ZIFACES_H
#define _ZIFACES_H

#include <libziaint.h>

#ifdef Z_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif

#ifdef Z_HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#include <stdint.h>


#define ZIFACE_NAME_SIZE 16
struct ziface_struct {
    char name[16];
    struct in_addr ip;
    struct in_addr netmask;
    unsigned char mac[6];
};

int zifaces_get(struct ziface_struct *ifaces, int max_interfaces, int up_only);

// 1 if input address is from local network
int ziface_is_local(struct in_addr ia);



struct zwifi_stat{
    uint8_t raw_level, raw_noise, raw_qual, raw_updated;
    uint8_t raw_max_qual;

    int quality_percent, level_dbm;
};

int ziface_wifi_stats(struct zwifi_stat *stat, char *devname, int clear);
char *ziface_macid(const char *namestarts);

#endif
