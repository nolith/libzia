/*
    Binary symbols lookup
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZBFD_H
#define __ZBFD_H

#include <libziaint.h>

#ifdef Z_HAVE_LIBBFD
#include <bfd.h>
#endif

#include <stdio.h>
#include <glib.h>

#ifdef Z_HAVE_WINSOCK2_H
#include <winsock2.h>
#endif
#ifdef Z_HAVE_WINDOWS_H
#include <windows.h>
#endif

#ifdef Z_MSC_MINGW
#ifdef Z_HAVE_DBGHELP_H
#include "dbghelp.h"
#else
#include <zdbghelp.h>
#endif
#endif


#ifdef Z_MSC_MINGW
typedef BOOL (WINAPI *T_SymFromAddr)(HANDLE, DWORD64, PDWORD64, PSYMBOL_INFO);
typedef BOOL (WINAPI *T_StackWalk64)(DWORD, HANDLE, HANDLE, LPSTACKFRAME64, PVOID, PREAD_PROCESS_MEMORY_ROUTINE64 ,
    PFUNCTION_TABLE_ACCESS_ROUTINE64, PGET_MODULE_BASE_ROUTINE64, PTRANSLATE_ADDRESS_ROUTINE64);
typedef BOOL (WINAPI *T_SymInitialize)(HANDLE, PCSTR, BOOL);
#endif


struct zbfd{
    GString *errstr;
    const char *filename;
    const char *functionname;
    unsigned int line;
	long offset;

#ifdef Z_HAVE_LIBBFD
    bfd *bfd;
    
    int found;
    bfd_vma pc;
    struct bfd_symbol **syms;
#endif
#ifdef Z_MSC_MINGW
	SYMBOL_INFO *symbol;
#endif

#ifdef Z_MSC_MINGW
	T_SymFromAddr SymFromAddr;
#endif
#if defined(Z_MINGW) || defined (Z_UNIX)
    FILE *dbin, *dzia;
    void *ziacodebase;
    char dsym[256];
#endif

};

struct zbfd *zbfd_init(void);
void zbfd_free(struct zbfd *zbfd);
int zbfd_open(struct zbfd *zbfd, char *filename, char *appddir);
int zbfd_lookup(struct zbfd *zbfd, void *addr);

    
int z_backtrace(void **buffer, int size, void *vex, void *ctx, int level);

#endif
