/*
    zchart.c - XY charts for debugging
    Copyright (C) 2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZCHART_H
#define __ZCHART_H

#include <libziaint.h>

#include <glib.h>

#ifdef Z_HAVE_SDL
#include <SDL.h>

struct zchart;

struct zchart_val{
	double x, y;
};

struct zchart_set {
	struct zchart *chart;
	GArray *values;
	char *desc;
	int color;
	double minx, maxx, miny, maxy;
	char *fmt;
};

struct zchart {
	SDL_Surface *surface;
	SDL_Rect area;
	int bgcolor;
    GPtrArray *sets;
	int mx, my;
};

struct zchart *zchart_init(SDL_Surface *surface, SDL_Rect *area, int bgcolor);
void zchart_free(struct zchart *);

struct zchart_set *zchart_add_set(struct zchart *chart, char *desc, int color);
void zchart_add(struct zchart *chart, int set, double x, double y);
void zchart_clear(struct zchart *chart);

void zchart_redraw(struct zchart *chart);
void zchart_mouse(struct zchart *chart, int mx, int my);			   

#endif
#endif
