/*
    zfhs.h - header for zfhs.c
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __FHS_H
#define __FHS_H

#include <libziaint.h>

int zfhs_lock(const char *device, int ignore_stale);
int zfhs_unlock(const char *device);
char *zfhs_strdup_error(int err, const char *device);



#endif
