/*
    portable time functions
    Copyright (C) 2009-2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/


#ifndef __ZTIME_H
#define __ZTIME_H

#include <libziaint.h>

#ifdef Z_HAVE_TIME_H
#include <time.h>
#endif

#ifdef Z_HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#ifndef Z_HAVE_SYS_TIME_H
int gettimeofday(struct timeval *tv, void *dummy);
#endif

#ifdef Z_MSC_MINGW
struct tm *gmtime_r(time_t *timep, struct tm *result);
struct tm *localtime_r(time_t *timep, struct tm *result);
time_t timegm(struct tm *tm);
int settimeofday(const struct timeval *tv, const void *zone);
#endif



#define ZTIMEOUT_MOD 10000000

int ztimeout_init(int timeout_ms);
int ztimeout_occured(int tmo);
int ztimeout_diff_ms(int *tmo);
//int ztimeout_test(int tmo, int i, int res);

/*
usage:
int tmo = ztimeout_init(miliseconds);
while(!z_timeout_occured(tmo)) { do_something(); }
*/


int zst_start(void);
int zst_stop(int reltime, const char *text);

void ST_START(void);
void ST_STOP(const char *text);
  
/*
usage:
int st = zst_start();
....
st = zst_stop(st, "prvni");
....
zst_stop(st, "druhy");

or

ST_START();
...
ST_STOP("prvni");
...
ST_STOP("druhy");

*/

double z_difftimeval_double(struct timeval *stop, struct timeval *start);
char *z_format_hms(char *s, int len, time_t t);


#endif
