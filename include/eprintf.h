/*
    libzia
    Copyright (C) 2011-2012  Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __EPRINTF_H
#define __EPRINTF_H

#include <libziaint.h>
#include <glib.h>

int zg_string_veprintfa(const char *flags, GString *gs, char *fmt, va_list l);
int zg_string_eprintfa(const char *flags, GString *gs, char *fmt, ...);
int zg_string_eprintf(const char *flags, GString *gs, char *fmt, ...);

int z_tokens(const char *str);
char *z_tokenize(char *str, int *tokenpos);

char *z_base64dec(char *bin, int maxlen, int *plen, char *src);
char *z_hexadec(char *bin, int maxlen, int *plen, char *src);

#endif /* __EPRINTF_H */     
