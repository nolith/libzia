
/*
    zptrarray.h - extended GPtrArray from glib
    Copyright (C) 2002-2013  Ladislav Vaiz <ok1zia@nagano.cz>
    and authors of glib www.gtk.org

*/


/* GLIB - Library of useful routines for C programming
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 * Modified by the GLib Team and others 1997-1999.  See the AUTHORS
 * file for a list of people on the GLib Team.  See the ChangeLog
 * files for a list of changes.  These files are distributed with
 * GLib at ftp://ftp.gtk.org/pub/gtk/. 
 */

#ifndef _ZPTRARRAY_H
#define _ZPTRARRAY_H

#include <libziaint.h>
#include <glib.h>

typedef struct _ZPtrArray ZPtrArray;
struct _ZPtrArray
{
  gpointer *pdata;
  guint     len;
};


/*void init_zptrarray(void);
void free_zptrarray(void);	*/
#define     z_ptr_array_index(array,index) (array->pdata)[index]
ZPtrArray* z_ptr_array_new (void);
void      z_ptr_array_free (ZPtrArray *array, gboolean free_seg);
void      z_ptr_array_free_all(ZPtrArray *array);
void      z_ptr_array_set_size (ZPtrArray *array, gint length);
gpointer  z_ptr_array_remove_index (ZPtrArray *array, guint index);
gpointer  z_ptr_array_remove_index_fast (ZPtrArray *array, guint index);
gboolean  z_ptr_array_remove (ZPtrArray *array, gpointer data);
gboolean  z_ptr_array_remove_fast (ZPtrArray *array, gpointer data);
void      z_ptr_array_add (ZPtrArray *array, gpointer data);
void	  z_ptr_array_insert (ZPtrArray *array, gpointer data, guint index);

#define zg_ptr_array_foreach(type, var, array) \
	int var##_i; \
    type var; \
	for (var##_i = 0, var = var##_i < (array)->len ? (type)g_ptr_array_index(array, var##_i) : NULL;  var##_i < (array)->len;  var##_i++, var = (type)g_ptr_array_index(array, var##_i))

#define zg_ptr_array_foreachback(type, var, array) \
    int var##_i; \
    type var; \
    for (var##_i = array->len - 1, var = var##_i >= 0 ? (type)g_ptr_array_index(array, var##_i) : NULL;  var##_i >= 0;  var##_i--, var = (type)g_ptr_array_index(array, var##_i))


void z_ptr_array_qsort (ZPtrArray *farray, int (*compar)(const void *, const void *));
void z_ptr_array_uniq (ZPtrArray *farray, int (*compar)(const void *, const void *), int free_dups);
gpointer *z_ptr_array_bsearch(ZPtrArray *farray, const void *key, int (*compar)(const void *, const void *));
gint z_ptr_array_bsearch_index(ZPtrArray *farray, const void *key, int (*compar)(const void *, const void *));



#endif
