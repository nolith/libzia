/*
    zgptrarray.h - libzia extensions for glib's GPtrArray
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZGPTRARRAY_H
#define __ZGPTRARRAY_H

#include <libziaint.h>
#include <glib.h>


void zg_ptr_array_free_all(GPtrArray *array);
void zg_ptr_array_free_items(GPtrArray *array);
void zg_ptr_array_qsort (GPtrArray *farray, int (*compar)(const void *, const void *));
int zg_ptr_array_find_str(GPtrArray *array, char *needle);


#endif
