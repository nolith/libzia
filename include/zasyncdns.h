/*
    zasyncdns.h - asynchronous DNS queries
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZASYNCDNS_H
#define __ZASYNCDNS_H

#include <libziaint.h>
#include <zsock.h>

#include <glib.h>

struct zselect;

struct zasyncdns{
    struct zselect *zsel;
    char *hostname;
    void *arg;
    int socktype;
    GThread *thread;
    void (*callback)(struct zasyncdns *adns, int n, int *family, int *socktype, int *protocol, int *addrlen, union zsockaddr *addr, char *errorstr);
};

struct zasyncdns *zasyncdns_init(void);
void zasyncdns_free(struct zasyncdns *adns);

struct zasyncdns *zasyncdns_getaddrinfo(
        struct zasyncdns *adns, 
        struct zselect *zsel, 
        void (*callback)(struct zasyncdns *adns, int n, int *family, int *socktype, int *protocol, int *addrlen, union zsockaddr *addr, char *errorstr),
        char *hostname,
        int socktype,
        void *arg);

void zasyncdns_read_handler(int n, char *items[]);

#endif

