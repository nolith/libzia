/*
    zgetopt.h - getopt compatibility layer
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZGETOPT_H
#define __ZGETOPT_H

#include <libziaint.h>

#ifdef Z_HAVE_GETOPT_H
#include <getopt.h>
#else

LIBZIA_API char *optarg;
LIBZIA_API int optind;
LIBZIA_API extern int opterr;
LIBZIA_API extern int optopt;

int getopt(int argc, char *argv[], char *optstring);

#endif
#endif
