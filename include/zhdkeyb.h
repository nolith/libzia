/*
    zhdkeyb.h - header for zhdkeyb.c
    Copyright (C) 2011-2021 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZHDKEYB_H
#define __ZHDKEYB_H

#include <libziaint.h>

#include <glib.h>

#ifdef Z_HAVE_LIBFTDI
#include <ftdi.h>
#endif

struct zselect;

#define Z_LINE_1 0
#define Z_LINE_2 0x40
#define Z_HDKEYB_LINES 2
#define Z_HDKEYB_CHARS 16

#define Z_HDKEYB_ROTARS 4
#define Z_HDKEYB_BUFLEN 32 

struct zhdkeyb{
#ifdef Z_HAVE_LIBFTDI
    struct ftdi_context *ftdi;
    GThread *thread;
    int qtf[Z_HDKEYB_ROTARS];
    int give_me_chance;
    int thread_break;
    char sbuf[Z_HDKEYB_BUFLEN], rbuf[Z_HDKEYB_BUFLEN];
    int slen, rlen;
    char wr, rd;
    char oldkeystate;
    char vram[Z_HDKEYB_LINES][Z_HDKEYB_CHARS], oldvram[Z_HDKEYB_LINES][Z_HDKEYB_CHARS];
    int actnr;
    char qtfstr[4];
    struct zselect *zsel;
#else
	int dummy;
#endif    
};

extern struct zhdkeyb *zhdkeyb;

struct zhdkeyb *zhdkeyb_init(struct zselect *zsel);
void zhdkeyb_free(struct zhdkeyb *zhdkeyb);
gpointer zhdkeyb_main(gpointer xxx);

void zhdkeyb_send(struct zhdkeyb *zhdkeyb);
int zhdkeyb_flush(struct zhdkeyb *zhdkeyb);
int zhdkeyb_reset(struct zhdkeyb *zhdkeyb);
int zhdkeyb_setdir(struct zhdkeyb *zhdkeyb);
int zhdkeyb_cmd_nowait(struct zhdkeyb *zhdkeyb, char c);
int zhdkeyb_write(struct zhdkeyb *zhdkeyb, char c);
int zhdkeyb_wait(struct zhdkeyb *zhdkeyb);
int zhdkeyb_cmd(struct zhdkeyb *zhdkeyb, char c);
#define zhdkeyb_cursor(zhdkeyb, c) zhdkeyb_cmd(zhdkeyb, 0x80|(c))
int zhdkeyb_data(struct zhdkeyb *zhdkeyb, char c);
void zhdkeyb_cgram(struct zhdkeyb *zhdkeyb, char pos);
void zhdkeyb_printc(struct zhdkeyb *zhdkeyb, char *str);
void zhdkeyb_printf(struct zhdkeyb *zhdkeyb, char line, char col, char *m, ...);
void zhdkeyb_print(struct zhdkeyb *zhdkeyb, char pos, char *str);
void zhdkeyb_clear(struct zhdkeyb *zhdkeyb);
char zhdkeyb_keyb_state(struct zhdkeyb *zhdkeyb);
char zhdkeyb_read_key(struct zhdkeyb *zhdkeyb);
int zhdkeyb_debug_pins(char a, char *t);


#endif
