/*
    ham callsign functions
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZCALL_H
#define __ZCALL_H

#include <libziaint.h>

char *z_get_raw_call(char *buf, const char *call);
int z_call_is_rover(const char *callsign);
char *z_suffix(char *call);

#define ZCBC_SLASH 1
#define ZCBC_MINUS 2
int z_can_be_call_char(const char c, int flags);
int z_can_be_call(const char *call, int flags);


#endif
