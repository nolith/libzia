/*
    zloc.c - WWL functions
    Copyright (C) 2011-2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZLOC_H
#define __ZLOC_H

#include <libziaint.h>

#define ZLOC_R_EARTH 6371.2907

double qth(char *qth,int width);
char *compute_wwl4(char *s, double h, double w);
char *mkwwl4(char *buf, int w, int h);
char *hw2loc(char *buf, double w, double h, int len);
char *x2gramin(char *buf, int size, double x, char *signs);
 
int qrbqtf(char *myqth,char *recqth,double *qrb,double *qtf,GString *gs_debug, int flags);
int hw2qrbqtf(double h1, double w1, double h2, double w2, double *qrb, double *qtf);
void hw2km(double h1, double w1, double h2, double w2, int *kx, int *ky);
void hw2km_d(double h1, double w1, double h2, double w2, double *kx, double *ky);
void hw2km_f(double h1, double w1, double h2, double w2, float *kx, float *ky);
void km2qrbqtf(int kx, int ky, double *qrb, double *qtf);

int qrbqtf2hw(double h1, double w1, double qrb, double qtf, double *h2, double *w2);
int qsopwr( char *myqth, char *recqth);
int qthwr(char *qth,int width);
void qrb_qtf_int(char *mywwl, char *wwl, int *qrb_int, int *qtf_int);
int iaru_round(double qrb);
void z_nearest_wwl(char *s, char *my);


#endif
