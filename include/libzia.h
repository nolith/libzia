/*
    libzia.h - header file for application
    Copyright (C) 2013-2022 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.
*/

#ifndef __LIBZIA_H
#define __LIBZIA_H

#ifdef _MSC_VER
#pragma warning(error:4013)
#endif

#include <eprintf.h>
#include <zandroid.h>
#include <zasyncdns.h>
#include <zavgfilter.h>
#include <zbat.h>
#include <zbfd.h>
#include <zbinbuf.h>
#include <zcall.h>
#include <zchart.h>
#include <zclip.h>
#include <zcor.h>
#include <zdebug.h>
#include <zdht11.h>
#include <zdir.h>
#include <zdump.h>
#include <zerror.h>
#include <zfhs.h>
#include <zfiledlg.h>
#include <zfile.h>
#include <zftdi.h>
#include <zgetopt.h>
#include <zghash.h>
#include <zglib.h>
#include <zgpio.h>
#include <zgptrarray.h>
#include <zhash.h>
#include <zhdkeyb.h>
#include <zhttp.h>
#include <zhttpd.h>
#include <zia.h>
#include <ziconv.h>
#include <zifaces.h>
#include <zinput.h>
#include <zjson.h>
#include <zlist.h>
#include <zloc.h>
#include <zmd5.h>
#include <zmisc.h>
#include <zmsgbox.h>
#include <zpath.h>
#include <zpng.h>
#include <zptrarray.h>
#include <zrc.h>
#include <zsdl.h>
#include <zselect.h>
#include <zserial.h>
#include <zsock.h>
#include <zstr.h>
#include <zsun.h>
#include <zthread.h>
#include <ztime.h>
#include <zver.h>

#ifdef Z_UNIX
#include <zbus.h>
#include <zi2c.h>
#include <zspidev.h>
#include <zspigpio.h>
#include <zssd1306.h>
#endif

#endif
