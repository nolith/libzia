/*
    zmsgbox.c - portable messagebox
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZMSGBOX_H
#define __ZMSGBOX_H

#include <libziaint.h>

#define ZMB_CANCEL 0
#define ZMB_OK     1
#define ZMB_YES    2
#define ZMB_NO     4


int z_msgbox_info(const char *caption, const char *fmt, ...);
int z_msgbox_error(const char *caption, const char *fmt, ...);

#endif
