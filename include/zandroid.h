/*
    zandroid.h - header for JNI calls
    Copyright (C) 2012-2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZANDROID_H
#define __ZANDROID_H

struct zselect;

void zandroid_init(struct zselect *location);
void zandroid_update_package(const char *filename);
void zandroid_browser(const char *url);
void zandroid_get_location(double *h, double *w, int *state);
void zandroid_play_wav(const char *filename);
void zandroid_ssbd_abort(void);
void zandroid_messagebox(const char *caption, const char *str);


#endif
