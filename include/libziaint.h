/*
    libzia.h
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __LIBZIAINT_H
#define __LIBZIAINT_H

#ifdef _MSC_VER
#pragma warning(error:4013)
#endif

#include <zconfig.h>

// LIBZIA_EXPORTS must be defined in libzia.vcxcproj
#ifndef _MSC_VER
#define LIBZIA_API
#elif defined(LIBZIA_EXPORTS)
#define LIBZIA_API __declspec(dllexport)
#elif defined(LIBZIA_STATIC_LINK)
#define LIBZIA_API
#else
#define LIBZIA_API __declspec(dllimport)
#endif

/* predefined macros of compilers

gcc unix:                __GNUC__=4.3.2
i586-mingw32msvc-gcc:    WIN32 __GNUC__=4.2.1 __MINGW32__
gcc cygwin:              __CYGWIN__ __GNUC__=3.4.4
gcc cyg #inc<windows>:   WIN32 __CYGWIN__ __GNUC__=3.4.4
gcc cygwin -mno-cygwin:  WIN32 __GNUC__=3.4.4 __MINGW32__
msvc6:                   WIN32 _MSC_VER=12.00
msvc10 (vs2010):         WIN32 _MSC_VER=16.00
*/

#if defined(_MSC_VER)
#define Z_MSC
#define Z_PLATFORM "msvc"

#ifdef _WIN64
#define Z_MACHINE "win64"
#else
#define Z_MACHINE "win32"
#endif

#elif defined(__MINGW32__)
#define Z_MINGW
// Z_PLATFORM defined in unix/zconfig.h
// Z_MACHINE defined in unix/zconfig.h
#elif defined(__CYGWIN__)
#define Z_CYGWIN
#define Z_PLATFORM "cygwin"
#define Z_MACHINE "win32"
#elif defined(ANDROID)
#define Z_ANDROID
#define Z_PLATFORM "android"
#define Z_MACHINE "arm"
#elif defined(__APPLE__)
// Z_MACOS is subset of Z_UNIX
#define Z_MACOS
#define Z_UNIX 
// Z_PLATFORM is "darwin"
// Z_MACHINE is "x86_64"
#else
#define Z_UNIX
// Z_PLATFORM defined in unix/zconfig.h
// Z_MACHINE defined in unix/zconfig.h
#endif

#if defined(Z_MSC) || defined(Z_MINGW)
// MSVC or MINGW
#define Z_MSC_MINGW
#endif

#if defined(Z_MSC) || defined(Z_MINGW) || defined(Z_CYGWIN)
#define Z_MSC_MINGW_CYGWIN
#endif

#if defined(Z_MSC) || defined(Z_MINGW) || defined(Z_ANDROID)
#define Z_MSC_MINGW_ANDROID
#endif

#if defined(Z_CYGWIN) || defined(Z_UNIX)
// UNIX API available
#define Z_UNIX_CYGWIN
#endif

#if defined(Z_UNIX) || defined(Z_MINGW)
#define Z_UNIX_MINGW
#endif

#if defined(Z_UNIX) || defined(Z_ANDROID)
#define Z_UNIX_ANDROID
#endif

#if defined(Z_UNIX) || defined(Z_CYGWIN) || defined(Z_ANDROID)
#define Z_UNIX_CYGWIN_ANDROID
#endif

#if defined(Z_UNIX) || defined(Z_MINGW) || defined(Z_ANDROID)
#define Z_UNIX_MINGW_ANDROID
#endif


#define Z_SWAP(typ, a, b) { typ c; c = a; a = b; b = c; }
#define Z_MAX(a, b) ( (a) > (b) ? (a) : (b) )
#define Z_MIN(a, b) ( (a) < (b) ? (a) : (b) )
#define Z_ABS(a) ( (a) < 0 ? (-(a)) : (a) )

int z_min3(int a, int b, int c);

#ifdef Z_CYGWIN
#ifndef _MAX_PATH
#define _MAX_PATH 260
#endif
#endif

#define ZRET(statement) if ((ret = statement) < 0) return ret;
#define ZRETX(statement) if ((ret = statement) < 0) goto x;

#endif
