
/*
    skel - Skeleton for new h files
    Copyright (C) 2011 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZSUN_H
#define __ZSUN_H

#include <libziaint.h>
#include <glib.h>

double zsun_riseset(time_t t, int sunrise, double latitude, double longitude);
gchar *zsun_strdup_riseset(time_t t, double latitude, double longitude);
void zsun_test(void);

#endif
