/*
    JSON library
    Copyright (C) 2014 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef __ZJSON_H
#define __ZJSON_H

#include <libziaint.h>
#include <glib.h>

#include <stdint.h>


void zjson_test(void);

// parse
char *zjson_get_str(const char *str, int len, const char *path);
int zjson_get_int(const char *str, int len, const char *path);


// create

void zjson_object_start(GString *gs, char *name);
void zjson_object_end(GString *gs);

void zjson_array_start(GString *gs, char *name);
void zjson_array_end(GString *gs);

void zjson_item_int(GString *gs, char *name, int value);
void zjson_item_int64(GString *gs, char *name, int64_t value);
void zjson_item_double(GString *gs, char *name, double value, int places);
void zjson_item_string(GString *gs, char *name, const char *value);
void zjson_item_bool(GString *gs, char *name, int value);
void zjson_item_null(GString *gs, char *name);
void zjson_strip(GString *gs);



#endif
