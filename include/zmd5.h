/*
    MD5 functions
    Copyright (C) 2012 Ladislav Vaiz <ok1zia@nagano.cz>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.

*/

#ifndef ZMD5_H
#define ZMD5_H

#include <libziaint.h>
#include <stdint.h>

struct zmd5 {
        uint32_t buf[4];
        uint32_t bits[2];
        unsigned char in[64];
		unsigned char tdigest[33];
};

void zmd5_init(struct zmd5 *ctx);
void zmd5_update(struct zmd5 *ctx, unsigned char *buf, unsigned len);
void zmd5_final(unsigned char digest[16], struct zmd5 *ctx);
unsigned char *zmd5_final_str(struct zmd5 *ctx);
void zmd5_transform(uint32_t buf[4], uint32_t in[16]);

#endif
