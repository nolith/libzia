#!/bin/sh

svnver=`svnversion -n . 2>/dev/null`

if test "$?" != "0"; then
    exit 0
fi 

if test "$svnver" = "exported"; then # svn 1.6
    exit 0
fi

if test "$svnver" = "Unversioned directory"; then # svn 1.8
   exit 0
fi

echo "#define Z_SVNVER \"$svnver\"" > include/zsvnversion.tmp
cmp -s include/zsvnversion.h include/zsvnversion.tmp
if test $? = 0; then
    echo Keep zsvnversion $svnver
    exit 0
fi

echo Updating zsvnversion to $svnver
mv include/zsvnversion.tmp include/zsvnversion.h

exit 0

 
